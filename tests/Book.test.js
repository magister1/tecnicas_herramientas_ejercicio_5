let Book = require("../model/Book.js");
let Review = require("../model/Review");

let hobbit = null;
beforeEach(() => {
	hobbit = new Book("El Hobbit", 2000);
    hobbit.addReview(4, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(1, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(3, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(5, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(4, "Vestibulum id ligula porta felis euismod semper.");

});

test("Should ", ()=>{
    expect(hobbit.getReviews().length).toBe(5);
})

test("Should return reviews length",()=>{
	expect(hobbit.getTotalReviews()).toBe(5);
})

test("Should check reviews length",()=>{
	expect(hobbit.getTotalReviews()).toBe(5);
})

test("Should check summation reviews",()=>{
	expect(hobbit.getSummationReviews()).toBe(17);
})

test("Should check average reviews",()=>{
	expect(hobbit.getReviewsAverage()).toBe(3.4);
})


