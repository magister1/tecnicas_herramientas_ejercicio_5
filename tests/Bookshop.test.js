let Bookshop = require("../model/Bookshop");
let ShoppingCart = require("../model/ShoppingCart");
let Purchase = require("../model/Purchase");
let Book = require("../model/Book");

let hobbit, castillo, aleph;
let cart1, cart2, cart3;
let purchase1, purchase2, purchase3;
let empty, shop;

beforeEach(() => {
    hobbit = new Book("El Hobbit", 2000);
    hobbit.addReview(4, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(1, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(3, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(2, "Vestibulum id ligula porta felis euismod semper.");
    hobbit.addReview(4, "Vestibulum id ligula porta felis euismod semper.");
    castillo = new Book("El Hombre en el Castillo", 1300);
    castillo.addReview(3, "Vestibulum id ligula porta felis euismod semper.");
    castillo.addReview(5, "Vestibulum id ligula porta felis euismod semper.");
    castillo.addReview(5, "Vestibulum id ligula porta felis euismod semper.");
    aleph = new Book("El Aleph", 2200);
    aleph.addReview(5, "Vestibulum id ligula porta felis euismod semper.");
    aleph.addReview(2, "Vestibulum id ligula porta felis euismod semper.");
    aleph.addReview(5, "Vestibulum id ligula porta felis euismod semper.");
    cart1 = new ShoppingCart();
    cart1.addBook(hobbit);
    cart1.addBook(castillo);
    cart2 = new ShoppingCart();
    cart2.addBook(aleph);
    cart2.addBook(aleph);
    cart3 = new ShoppingCart();
    cart3.addBook(aleph);
    cart3.addBook(castillo);
    purchase1 = new Purchase(cart1);
    purchase2 = new Purchase(cart2);
    purchase3 = new Purchase(cart3);
    shop = new Bookshop();
    shop.addPurchase(purchase1);
    shop.addPurchase(purchase2);
    shop.addPurchase(purchase3);
    empty = new Bookshop();
});

test("Should return the most expensive purchase", ()=>{
    expect( () => {empty.topPurchase()} ).toThrow("There are no purchases");
    expect(shop.topPurchase()).toBe(purchase2);
})

test("Should return getBestRatedBook", ()=>{
    expect(shop.getBestRatedBook()).toBe(castillo);
})

test("Should return top grossing", ()=>{
    expect(shop.topGrossing()).toBe(aleph);
})

test("Should return best seller", ()=>{
    expect(shop.bestSeller()).toBe(aleph);
})

test("Should check unique purchased books", ()=>{
	expect(Object.keys(shop.getUniquePurchasedBooks()).length).toBe(3);
	expect(shop.getUniquePurchasedBooks()[aleph.name]).toBe(aleph);
	expect(shop.getUniquePurchasedBooks()[castillo.name]).toBe(castillo);
	expect(shop.getUniquePurchasedBooks()[hobbit.name]).toBe(hobbit);
})

test("Should check lenght purchased books", ()=>{
	expect(Object.keys(shop.getAllPurchasedBooks()).length).toBe(6);
})

test("Should check getBookByName get valid book", ()=>{
	expect(shop.getBookByName("El Hobbit")).toBe(hobbit);

})

